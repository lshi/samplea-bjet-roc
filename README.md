# sampleA-bjet-ROC
Create awesome ROC curves for sample A validation rounds. This should run on lxplus (if not create an issue), but locally it should work too (but you're on your own then).

## 🎶 This is how we do it 🎶

1. Select the ttbar samples from the reference (usually the previous validation round) and the current release as indicated in the JIRA ticket
2. Clone the [TDD/dumpster](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper) and follow the README to install.
    - Instead of sourcing `analysisbase.sh` during install you might have to actually source `athena.sh` since AnalysisBase did not work for me last time I tried.
3. $ source training-dataset-dumper/FTagDumper/grid/setup.sh
4. Point config to the samples you just selected and maybe change the variables to write out:
    - training-dataset-dumper/FTagDumper/grid/inputs/trigger-val.txt
    - training-dataset-dumper/configs/trigger_emtopo_SampleA.json
    - training-dataset-dumper/configs/trigger_pflow_SampleA.json
5. $ grid-submit trigger-val
    - After succesful submission wait for the grid to generate your h5 file. When finished, download it to your current lxplus node.
6. Edit `rocs.py` to run on the resulting h5 files
    - Add your downloaded h5 file somewhere around line 60 as an H5Sample, and point the variables `sample_ref` and `sample_val` to them.
    - If the bjet trigger moved on beyond the taggers that are plotted in the current code, please create an MR to update them.
    - Running this script requires `puma` to be installed, and maybe some more (please create an issue for any missing package while running this script)
        - https://github.com/umami-hep/puma, pip install puma-hep
