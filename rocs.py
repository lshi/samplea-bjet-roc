#!/usr/bin/env python
"""Produce roc curves from tagger output and labels."""
import h5py
import numpy as np
import pandas as pd

from puma import Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import logger

# The line below generates dummy data which is similar to a NN output
#df = get_dummy_2_taggers()

branch_list = {"emtopo": 
        [
        "fastDips_pb",
        "fastDips_pc",
        "fastDips_pu",
        "fastGN120230327_pb",
        "fastGN120230327_pc",
        "fastGN120230327_pu",
        "fastGN220240122_pb",
        "fastGN220240122_pc",
        "fastGN220240122_pu",
        "HadronConeExclTruthLabelID",
        ],
        "pflow": 
        [
        "fastDips_pb",
        "fastDips_pc",
        "fastDips_pu",
        "GN220240122_pb",
        "GN220240122_pc",
        "GN220240122_pu",
        "GN120220813_pb",
        "GN120220813_pc",
        "GN120220813_pu",
        "DL1d20211216_pb",
        "DL1d20211216_pc",
        "DL1d20211216_pu",
        "HadronConeExclTruthLabelID",
        ]
}


# read from input
def open_h5(file_name, jet_type=None, branch_list=branch_list["emtopo"]):
    with h5py.File(file_name, "r") as f:
        print(f.keys())
        if jet_type is None:
            print(f['jets'])
            df = pd.DataFrame(
                    f["jets"].fields(branch_list)[:-1]
                    )
        else:
            print(f[jet_type].keys())
            df = pd.DataFrame(
                    f[jet_type]["jets"].fields(branch_list)[:-1]
                    )
    return df

from collections import namedtuple
H5Sample = namedtuple('H5Sample', 'label file')
#sample_r15148 = H5Sample('SampleA 31st round r15148', 'user.lbeemste.601229.e8514_e8528_s4162_s4114_r15148.tdd.trigger_all_SampleA.24_0_21.Valentine2024-19-g46050aa_output.h5/user.lbeemste.37673060._000001.output.h5')
#sample_r15182 = H5Sample('SampleA 32nd round r15182', 'user.lbeemste.601229.e8514_e8528_s4162_s4114_r15182.tdd.trigger_all_SampleA.24_0_21.Valentine2024-19-g90131ab_output.h5/user.lbeemste.37683630._000001.output.h5')
#sample_r15272 = H5Sample('SampleA 33nd round r15272', 'user.lbeemste.601229.e8514_e8528_s4162_s4114_r15272.tdd.trigger_all_SampleA.24_0_21.Valentine2024-19-gb86d5d4_output.h5/user.lbeemste.37683804._000001.output.h5')
#sample_r15281 = H5Sample('SampleA 33nd round r15281', 'user.lbeemste.601229.e8514_e8528_s4162_s4114_r15281.tdd.trigger_all_SampleA.24_0_21.Valentine2024-19-gb86d5d4_output.h5/user.lbeemste.37683803._000001.output.h5')

#sample_r15272 = H5Sample('SampleA 33rd round r15272', 'user.lshi.601229.e8514_e8528_s4162_s4114_r15272.tdd.trigger_all_SampleA.25_0_6.Valentine2024-69-gb177271_output.h5/user.lshi.38933268._000001.output.h5')
#sample_r15424 = H5Sample('SampleA 34nd round r15424 MC23a', 'user.lshi.601229.e8514_e8528_s4162_s4114_r15424.tdd.trigger_all_SampleA.25_0_6.Valentine2024-70-g33b80cc_output.h5/user.lshi.38978623._000001.output.h5')
#sample_r15425 = H5Sample('SampleA 34nd round r15425 MC23d', 'user.lshi.601229.e8514_e8528_s4159_s4114_r15425.tdd.trigger_all_SampleA.25_0_6.Valentine2024-70-g33b80cc_output.h5/user.lshi.38978624._000001.output.h5')

sample_r15272 = H5Sample('SampleA 33rd round r15272', 'user.lshi.601229.e8514_e8528_s4162_s4114_r15272.tdd.trigger_all_SampleA.25_0_5.Valentine2024-71-gbe64ca2_output.h5/user.lshi.39751894._000001.output.h5')
sample_r15424 = H5Sample('SampleA 34nd round r15424 MC23a', 'user.lshi.601229.e8514_e8528_s4162_s4114_r15424.tdd.trigger_all_SampleA.25_0_5.Valentine2024-72-g3bcdacc_output.h5/user.lshi.39752814._000001.output.h5')
sample_r15425 = H5Sample('SampleA 34nd round r15425 MC23d', 'user.lshi.601229.e8514_e8528_s4159_s4114_r15425.tdd.trigger_all_SampleA.25_0_5.Valentine2024-72-g3bcdacc_output.h5/user.lshi.39752813._000001.output.h5')

sample_ref = sample_r15424
sample_val = sample_r15425
dfref_emtopo = open_h5(sample_ref.file, jet_type='emtopo', branch_list=branch_list['emtopo'])
dfval_emtopo = open_h5(sample_val.file, jet_type='emtopo', branch_list=branch_list['emtopo'])

dfref_pflow = open_h5(sample_ref.file, jet_type='pflow', branch_list=branch_list['pflow'])
dfval_pflow = open_h5(sample_val.file, jet_type='pflow', branch_list=branch_list['pflow'])

logger.info("caclulate tagger discriminants")


# define a small function to calculate discriminant
def disc_fct(arr: np.ndarray, f_c: float = 0.018) -> np.ndarray:
    """Tagger discriminant.

    Parameters
    ----------
    arr : numpy.ndarray
        array with with shape (, 3)
    f_c : float, optional
        f_c value in the discriminant (weight for c-jets rejection)

    Returns
    -------
    np.ndarray
        Array with the discriminant values inside.
    """
    # you can adapt this for your needs
    if arr[2]>0:
        return np.log(arr[2] / (f_c * arr[1] + (1 - f_c) * arr[0]))
    else:
        return np.log(1e-100) # for fGN1 on jets <= 1 track


# you can also use a lambda function
#discs_fastGN1 = np.apply_along_axis(
#    lambda a: np.log(a[2] / (0.018 * a[1] + (1 - 0.018) * a[0])),
#    1,
#    df[["fastGN120230327_pu", "fastGN120230327_pc", "fastGN120230327_pb"]].values,
#)

# calculate discriminant
discs_fastGN2_ref = np.apply_along_axis(
    disc_fct, 1, dfref_emtopo[["fastGN220240122_pu", "fastGN220240122_pc", "fastGN220240122_pb"]].values
)
discs_fastGN1_ref = np.apply_along_axis(
    disc_fct, 1, dfref_emtopo[["fastGN120230327_pu", "fastGN120230327_pc", "fastGN120230327_pb"]].values
)
discs_fastdips_ref = np.apply_along_axis(
    disc_fct, 1, dfref_emtopo[["fastDips_pu", "fastDips_pc", "fastDips_pb"]].values
)
discs_fastGN2_val = np.apply_along_axis(
    disc_fct, 1, dfval_emtopo[["fastGN220240122_pu", "fastGN220240122_pc", "fastGN220240122_pb"]].values
)
discs_fastGN1_val = np.apply_along_axis(
    disc_fct, 1, dfval_emtopo[["fastGN120230327_pu", "fastGN120230327_pc", "fastGN120230327_pb"]].values
)
discs_fastdips_val = np.apply_along_axis(
    disc_fct, 1, dfval_emtopo[["fastDips_pu", "fastDips_pc", "fastDips_pb"]].values
)

discs_GN2_ref = np.apply_along_axis(
    disc_fct, 1, dfref_pflow[["GN220240122_pu", "GN220240122_pc", "GN220240122_pb"]].values
)

discs_GN1_ref = np.apply_along_axis(
    disc_fct, 1, dfref_pflow[["GN120220813_pu", "GN120220813_pc", "GN120220813_pb"]].values
)
discs_DL1d_ref = np.apply_along_axis(
    disc_fct, 1, dfref_pflow[["DL1d20211216_pu", "DL1d20211216_pc", "DL1d20211216_pb"]].values
)
discs_GN2_val = np.apply_along_axis(
    disc_fct, 1, dfval_pflow[["GN220240122_pu", "GN220240122_pc", "GN220240122_pb"]].values
)
discs_GN1_val = np.apply_along_axis(
    disc_fct, 1, dfval_pflow[["GN120220813_pu", "GN120220813_pc", "GN120220813_pb"]].values
)
discs_DL1d_val = np.apply_along_axis(
    disc_fct, 1, dfval_pflow[["DL1d20211216_pu", "DL1d20211216_pc", "DL1d20211216_pb"]].values
)


# defining target efficiency
sig_eff = np.linspace(0.59, 1, 20)
# defining boolean arrays to select the different flavour classes
is_light_emtopo_ref = dfref_emtopo["HadronConeExclTruthLabelID"] == 0
is_c_emtopo_ref = dfref_emtopo["HadronConeExclTruthLabelID"] == 4
is_b_emtopo_ref = dfref_emtopo["HadronConeExclTruthLabelID"] == 5

n_jets_light_emtopo_ref = sum(is_light_emtopo_ref)
n_jets_c_emtopo_ref = sum(is_c_emtopo_ref)

is_light_pflow_ref = dfref_pflow["HadronConeExclTruthLabelID"] == 0
is_c_pflow_ref = dfref_pflow["HadronConeExclTruthLabelID"] == 4
is_b_pflow_ref = dfref_pflow["HadronConeExclTruthLabelID"] == 5

n_jets_light_pflow_ref = sum(is_light_pflow_ref)
n_jets_c_pflow_ref = sum(is_c_pflow_ref)

logger.info("Calculate rejection ref")
fastGN2_ujets_rej_ref = calc_rej(discs_fastGN2_ref[is_b_emtopo_ref], discs_fastGN2_ref[is_light_emtopo_ref], sig_eff)
fastGN2_cjets_rej_ref = calc_rej(discs_fastGN2_ref[is_b_emtopo_ref], discs_fastGN2_ref[is_c_emtopo_ref], sig_eff)
fastGN1_ujets_rej_ref = calc_rej(discs_fastGN1_ref[is_b_emtopo_ref], discs_fastGN1_ref[is_light_emtopo_ref], sig_eff)
fastGN1_cjets_rej_ref = calc_rej(discs_fastGN1_ref[is_b_emtopo_ref], discs_fastGN1_ref[is_c_emtopo_ref], sig_eff)
fastdips_ujets_rej_ref = calc_rej(discs_fastdips_ref[is_b_emtopo_ref], discs_fastdips_ref[is_light_emtopo_ref], sig_eff)
fastdips_cjets_rej_ref = calc_rej(discs_fastdips_ref[is_b_emtopo_ref], discs_fastdips_ref[is_c_emtopo_ref], sig_eff)
GN2_ujets_rej_ref = calc_rej(discs_GN2_ref[is_b_pflow_ref], discs_GN2_ref[is_light_pflow_ref], sig_eff)
GN2_cjets_rej_ref = calc_rej(discs_GN2_ref[is_b_pflow_ref], discs_GN2_ref[is_c_pflow_ref], sig_eff)
GN1_ujets_rej_ref = calc_rej(discs_GN1_ref[is_b_pflow_ref], discs_GN1_ref[is_light_pflow_ref], sig_eff)
GN1_cjets_rej_ref = calc_rej(discs_GN1_ref[is_b_pflow_ref], discs_GN1_ref[is_c_pflow_ref], sig_eff)
DL1d_ujets_rej_ref = calc_rej(discs_DL1d_ref[is_b_pflow_ref], discs_DL1d_ref[is_light_pflow_ref], sig_eff)
DL1d_cjets_rej_ref = calc_rej(discs_DL1d_ref[is_b_pflow_ref], discs_DL1d_ref[is_c_pflow_ref], sig_eff)

#val
is_light_emtopo_val = dfval_emtopo["HadronConeExclTruthLabelID"] == 0
is_c_emtopo_val = dfval_emtopo["HadronConeExclTruthLabelID"] == 4
is_b_emtopo_val = dfval_emtopo["HadronConeExclTruthLabelID"] == 5

n_jets_light_emtopo_val = sum(is_light_emtopo_val)
n_jets_c_emtopo_val = sum(is_c_emtopo_val)

is_light_pflow_val = dfval_pflow["HadronConeExclTruthLabelID"] == 0
is_c_pflow_val = dfval_pflow["HadronConeExclTruthLabelID"] == 4
is_b_pflow_val = dfval_pflow["HadronConeExclTruthLabelID"] == 5

n_jets_light_pflow_val = sum(is_light_pflow_val)
n_jets_c_pflow_val = sum(is_c_pflow_val)

logger.info("Calculate rejection val")
fastGN2_ujets_rej_val = calc_rej(discs_fastGN2_val[is_b_emtopo_val], discs_fastGN2_val[is_light_emtopo_val], sig_eff)
fastGN2_cjets_rej_val = calc_rej(discs_fastGN2_val[is_b_emtopo_val], discs_fastGN2_val[is_c_emtopo_val], sig_eff)
fastGN1_ujets_rej_val = calc_rej(discs_fastGN1_val[is_b_emtopo_val], discs_fastGN1_val[is_light_emtopo_val], sig_eff)
fastGN1_cjets_rej_val = calc_rej(discs_fastGN1_val[is_b_emtopo_val], discs_fastGN1_val[is_c_emtopo_val], sig_eff)
fastdips_ujets_rej_val = calc_rej(discs_fastdips_val[is_b_emtopo_val], discs_fastdips_val[is_light_emtopo_val], sig_eff)
fastdips_cjets_rej_val = calc_rej(discs_fastdips_val[is_b_emtopo_val], discs_fastdips_val[is_c_emtopo_val], sig_eff)
GN2_ujets_rej_val = calc_rej(discs_GN2_val[is_b_pflow_val], discs_GN2_val[is_light_pflow_val], sig_eff)
GN2_cjets_rej_val = calc_rej(discs_GN2_val[is_b_pflow_val], discs_GN2_val[is_c_pflow_val], sig_eff)
GN1_ujets_rej_val = calc_rej(discs_GN1_val[is_b_pflow_val], discs_GN1_val[is_light_pflow_val], sig_eff)
GN1_cjets_rej_val = calc_rej(discs_GN1_val[is_b_pflow_val], discs_GN1_val[is_c_pflow_val], sig_eff)
DL1d_ujets_rej_val = calc_rej(discs_DL1d_val[is_b_pflow_val], discs_DL1d_val[is_light_pflow_val], sig_eff)
DL1d_cjets_rej_val = calc_rej(discs_DL1d_val[is_b_pflow_val], discs_DL1d_val[is_c_pflow_val], sig_eff)

# Alternatively you can simply use a results file with the rejection values
# logger.info("read h5")
# df = pd.read_hdf("results-rej_per_eff-1_new.h5", "ttbar")
# print(df.columns.values)
# sig_eff = df["effs"]
# fastGN1_ujets_rej = df["fastGN1_ujets_rej"]
# fastGN1_cjets_rej = df["fastGN1_cjets_rej"]
# fastdips_ujets_rej = df["fastdips_ujets_rej"]
# fastdips_cjets_rej = df["fastdips_cjets_rej"]
# n_test = 10_000

# here the plotting of the roc starts
logger.info("Plotting ROC curves.")
plot_roc = RocPlot(
    n_ratio_panels=1,
    ylabel="Background rejection",
    xlabel="$b$-jet efficiency",
    atlas_second_tag="$\\sqrt{s}=13.6$ TeV, $t\\bar{t}}$",
    figsize=(6.5, 6),
    y_scale=1.4,
)
plot_roc.add_roc(Roc(np.asarray([1]),np.asarray([1]), label=sample_ref.label, linestyle="dashed", colour='black'))
plot_roc.add_roc(Roc(np.asarray([1]),np.asarray([1]), label=sample_val.label, linestyle="solid", colour='black'))

plot_roc.add_roc(
    Roc(
        sig_eff,
        GN2_ujets_rej_ref,
        n_test=n_jets_light_pflow_ref,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="GN2",
        linestyle="dashed",
        colour="orange",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        GN2_ujets_rej_val,
        n_test=n_jets_light_pflow_val,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="GN2",
        label="PFlow | GN2",
        linestyle="solid",
        colour="orange",
    ),
)


plot_roc.add_roc(
    Roc(
        sig_eff,
        GN1_ujets_rej_ref,
        n_test=n_jets_light_pflow_ref,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="GN1",
        linestyle="dashed",
        colour="red",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        GN1_ujets_rej_val,
        n_test=n_jets_light_pflow_val,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="GN1",
        label="PFlow | GN1",
        linestyle="solid",
        colour="red",
    ),
)

plot_roc.add_roc(
    Roc(
        sig_eff,
        DL1d_ujets_rej_ref,
        n_test=n_jets_light_pflow_ref,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="DL1d",
        linestyle="dashed",
        colour="blue",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        DL1d_ujets_rej_val,
        n_test=n_jets_light_pflow_val,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="DL1d",
        label="PFlow | DL1d",
        linestyle="solid",
        colour="blue",
    ),
)

plot_roc.add_roc(
    Roc(
        sig_eff,
        fastGN2_ujets_rej_ref,
        n_test=n_jets_light_emtopo_ref,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="fastGN2",
        linestyle="dashed",
        colour="cyan",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        fastGN2_ujets_rej_val,
        n_test=n_jets_light_emtopo_val,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="fastGN2",
        label="EMTopo | fastGN2",
        linestyle="solid",
        colour="cyan",
    ),
)

plot_roc.add_roc(
    Roc(
        sig_eff,
        fastGN1_ujets_rej_ref,
        n_test=n_jets_light_emtopo_ref,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="fastGN1",
        linestyle="dashed",
        colour="lime",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        fastGN1_ujets_rej_val,
        n_test=n_jets_light_emtopo_val,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="fastGN1",
        label="EMTopo | fastGN1",
        linestyle="solid",
        colour="lime",
    ),
)

plot_roc.add_roc(
    Roc(
        sig_eff,
        fastdips_ujets_rej_ref,
        n_test=n_jets_light_emtopo_ref,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="fastDIPS",
        linestyle="dashed",
        colour="green",
    ),
    reference=True,
)
plot_roc.add_roc(
    Roc(
        sig_eff,
        fastdips_ujets_rej_val,
        n_test=n_jets_light_emtopo_val,
        rej_class="ujets",
        signal_class="bjets",
        ratio_group="fastDIPS",
        label="EMTopo | fastDIPS",
        linestyle="solid",
        colour="green",
    ),
)


# setting which flavour rejection ratio is drawn in which ratio panel
plot_roc.set_ratio_class(1, "ujets")
#plot_roc.set_ratio_class(2, "cjets")
# if you want to swap the ratios just uncomment the following 2 lines
# plot_roc.set_ratio_class(2, "ujets")
# plot_roc.set_ratio_class(1, "cjets")

plot_roc.draw()
plot_roc.savefig("roc.png", transparent=False)
